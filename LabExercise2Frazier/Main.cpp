#include <iostream>
#include <conio.h>

using namespace  std;

enum Rank {TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE};

enum Suit { HEARTS, SPADES, CLUBS, DIAMONDS };

struct Card {
	Rank rank;
	Suit suit;
};

void PrintCard(Card card)
{
	cout << "The ";
	switch (card.rank)
	{
		case TWO: cout << "Two"; break;
		case THREE: cout << "Three"; break;
		case FOUR: cout << "Four"; break;
		case FIVE: cout << "Five"; break;
		case SIX: cout << "Six"; break;
		case SEVEN: cout << "Seven"; break;
		case EIGHT: cout << "Eight"; break;
		case NINE: cout << "Nine"; break;
		case TEN: cout << "Ten"; break;
		case JACK: cout << "Jack"; break;
		case QUEEN: cout << "Queen"; break;
		case KING: cout << "King"; break;
		case ACE: cout << "Ace"; break;

	}
	switch (card.suit) {

		
		case HEARTS: cout << " of Hearts\n"; break;
		case CLUBS: cout << " of Clubs\n"; break;
		case SPADES: cout << " of Spades\n"; break;
		case DIAMONDS: cout << " of Diamonds\n"; break;
	}
	
}

Card HighCard(Card c1, Card c2) 
{
	if (c1.rank > c2.rank)
	{
		return c1;
		return c2;
	}
}

int main() {

	Card c;
	c.rank = NINE;
	c.suit = CLUBS;

	Card c2;
	c2.rank = TEN;
	c2.suit = HEARTS;

	PrintCard(HighCard(c, c2));


}